extends "res://controls/ControlBase.gd"

func _process(delta):
	if Input.is_action_pressed("action_umbrella"):
		action_umbrella = true
	else:
		action_umbrella = false
		
	if Input.is_action_just_pressed("action_jump"):
		action_jump = true
	else:
		action_jump = false
	
	action_direction = Vector2(0,0)
	if Input.is_action_pressed("action_up"):
		action_direction.y += 1
	if Input.is_action_pressed("action_left"):
		action_direction.x -= 1
	if Input.is_action_pressed("action_right"):
		action_direction.x += 1
	if Input.is_action_pressed("action_down"):
		action_direction.y -= 1