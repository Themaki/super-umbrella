extends Node2D

var taken=false

func _on_coin_collected(body):
	if not taken:
		taken = true
		body.get_node("Collector").coins += 1
		$CoinCounter.set_text(str(body.get_node("Collector").coins))
		$Sprite.queue_free()
		$Timer.start()


func _on_Timer_timeout():
	get_parent().queue_free()
