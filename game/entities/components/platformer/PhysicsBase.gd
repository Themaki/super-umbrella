extends Node

# Physics constants
export(float) var terminal_hor_speed = 200.0 # maximum horizont speed
export(float) var terminal_ver_speed = 1000.0   # maximum vertical speed
export(float) var acceleration       = 15.0   # horizontal acceleration
export(float) var deceleration       = 60.0  # horizontal friction
export(float) var drag               = 0.01 # vertical "friction" air resistence
export(Vector2) var up               = Vector2(0, -1)

# States with higher priority override lower priority ones
export(int) var priority = 1

# Tell whether this component wants to become the active physics state
func _requests_control(entity):
	return false
