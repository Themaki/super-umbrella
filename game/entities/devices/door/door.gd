extends StaticBody2D

export(bool) var toggle = false
export(bool) var closed = true

func _ready():
	if not closed:
		hide()
		$CollisionShape2D.set_disabled(true)

func _open():
	if toggle:
		if closed:
			hide()
			$CollisionShape2D.set_disabled(true)
		else:
			show()
			$CollisionShape2D.set_disabled(false)
		closed = not closed
	else:
		queue_free()