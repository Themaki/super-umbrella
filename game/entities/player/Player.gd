extends KinematicBody2D

const GRAVITY_default  =  2400
const TERMINAL_default =  1000
var velocity   = Vector2()
const UP       = Vector2(0,-1)
const JMP_STR  = -750
var   JMP_MPL  = 1
const ACCELERATION=10
const DRAG = 30
const MAX_SPEED=  150
const UMB_STR  =  6
const GRAVITY  = GRAVITY_default
const TERMINAL = TERMINAL_default

const GROUND_DRAG = 30
const AIR_DRAG    = 0.5
const GROUND_ACCL = 15
const AIR_ACCL    = 10

func _ready():
	for child in get_children():
		if child.has_method("_entity_ready"):
			child._entity_ready(self)

func _process(delta):
	if $States.onFloor:
		ACCELERATION = GROUND_ACCL
		DRAG         = GROUND_DRAG
	else:
		ACCELERATION = AIR_ACCL
		DRAG         = AIR_DRAG
	
	if $States.umbrellaActive:
		if $umbrella_sprite/umbrella_animations.get_current_animation() != "falling":
			$umbrella_sprite/umbrella_animations.play("falling")
	else:
		$umbrella_sprite.frame = 0
	
	if $States.lookingRight:
		$player_sprite.z_index   = 0
		$umbrella_sprite.z_index = 1
	else:
		$player_sprite.z_index   = 1
		$umbrella_sprite.z_index = 0

func _physics_process(delta):
	# Updates isMoving state
	if velocity.length() == 0:
		$States.isMoving = false
	else:
		$States.isMoving = true

	# Updates onAir state
	if is_on_floor():
		$States.onFloor = true
	else:
		$States.onFloor = false

	# Updates lookingRight state
	if   velocity.x > 0:
		$States.lookingRight = true
	elif velocity.x < 0:
		$States.lookingRight = false
	else:
		pass #keep looking at where you were
	
	# Updates umbrellaActive state
	if not $States.onFloor and $Control.action_umbrella:
		if (velocity.y > 0): #is going down, y is positive
			$States.umbrellaActive = true
		else:
			$States.umbrellaActive = false
	else:
		$States.umbrellaActive = false
	
	# Handles Jump Behaviour
	if $States.onFloor and $Control.action_jump:
		velocity.y = JMP_STR*JMP_MPL

	# Handles Horizontal Movement
	velocity.x += ACCELERATION*$Control.action_direction.x
	
	velocity.x = max(min(velocity.x, MAX_SPEED), -MAX_SPEED)
	
	if abs($Control.action_direction.x) < 0.3:# for controller
		velocity.x -= DRAG * (velocity.x/MAX_SPEED)
	
	# Handles Umbrella Behaviour
	if $States.umbrellaActive and not $States.onFloor:
		GRAVITY  = GRAVITY_default  / UMB_STR
		TERMINAL = TERMINAL_default / UMB_STR
	else:
		GRAVITY  = GRAVITY_default
		TERMINAL = TERMINAL_default

	# Applies GRAVITY
	velocity.y += GRAVITY*delta

	# Clamp velocity to Terminal Velocity and move
	if (velocity.y > 0): #only limit it when falling
		velocity.y = max(min(velocity.y, TERMINAL), -TERMINAL)
	
	velocity = move_and_slide(velocity, UP)

func _kill():
	for child in get_children():
		if child is Camera2D:
			remove_child(child)
			child.position = child.get_camera_position()
			get_parent().add_child(child)
	queue_free()
