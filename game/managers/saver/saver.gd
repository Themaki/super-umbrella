var save_path = "user://save.save" #stage saves file

var save_file = File.new()
var flags = [] #flags to save

#Methods that handle stage_save files
func save_stage(stage_name):
	var save

	#Save Stage Completion
	if save_file.file_exists(save_path):
		save_file.open(save_path,File.READ_WRITE)

		save = parse_json(save_file.get_as_text())
		save[stage_name] = "compleat"

	else:
		save_file.open(save_path,File.WRITE)
		save = {stage_name : "compleat"}

	#Save flags
	while flags.size() > 0:
		var flag = flags.pop_back()
		save[flag] = "done"

	save_file.store_string(to_json(save))
	save_file.close()

func has_save():
	return save_file.file_exists(save_path)

func load_stage():
	assert(save_file.file_exists(save_path))

	save_file.open(save_path,File.READ)
	var save = parse_json(save_file.get_as_text())
	save_file.close()

	return save

# prepares a flag to be saved when the stage is finished
# Flags are named "flag_flagname" to avoid getting them mixed up with stages
func prepare_flag(flag):
	flags.append("flag_"+flag)